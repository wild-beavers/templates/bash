#!/usr/bin/env bash

set -e

# ############### #
# Default Globals #
# ############### #

APPLICATION_NAME="examples"

# ######### #
# Functions #
# ######### #

display_help() {
  echo "$APPLICATION_NAME"
  echo " "
  echo "$APPLICATION_NAME [options]"
  echo " "
  echo "options:"
  echo " "
  exit 0
}

process_arguments() {
  while test $# -gt 0; do
    case "$1" in
    -h | --help)
      display_help
      ;;
    -v)
      set -x
      shift
      ;;
    *)
      print_error "\e[1m$1\e[22m is not a valid option."
      exit 1
      ;;
    esac
  done
}

# #### #
# Main #
# #### #

main() {
  process_arguments "$@"
}

main "$@"
